import unittest
from calculator import Sum, Subtraction, Multiplication, Division


# Unit Test for Calculator Operations
class TestCalculator(unittest.TestCase):
    def test_sum(self):
        """Simple Test for addition operation"""
        value1 = 5
        value2 = 6
        result_value = 11
        result_test_op = Sum().execute(value1, value2)
        self.assertEqual(result_test_op, result_value)

    def test_subtraction(self):
        """Simple Test for subtraction operation"""
        value1 = 8
        value2 = 2
        result_value = 6
        result_test_op = Subtraction().execute(value1, value2)
        self.assertEqual(result_test_op, result_value)

    def test_multiplication(self):
        """Simple test for multiplication operation"""
        value1 = 5
        value2 = 8
        result_value = 40
        result_test_op = Multiplication().execute(value1, value2)
        self.assertEqual(result_test_op, result_value)

    def test_division(self):
        """Simple division for division operation"""
        value1 = 45
        value2 = 5
        result_value = 9
        result_test_op = Division().execute(value1, value2)
        self.assertEqual(result_test_op, result_value)

    def test_division_by_zero(self):
        """Test for division operation by zero"""
        value1 = 5
        value2 = 0
        with self.assertRaises(ZeroDivisionError):
            Division().execute(value1, value2)


if __name__ == '__main__':
    unittest.main()
