from calculator import *

# Repeated execution of operations, users can indicate operation selecting by option number
while True:
    try:
        print("You must select one of the following options")
        print("1. Sum")
        print("2. Subtraction")
        print("3. Multiplication")
        print("4. Division")
        print("0. Exit")
        option_selected = int(input("Enter the option number you want to use:"))
        if option_selected == 0:
            print("End of program")
            break
        elif 1 <= option_selected <= 4:
            param1 = float(input("First Value:"))
            param2 = float(input("Second Value:"))

            if option_selected == 1:
                op_sum = Sum()
                execute_operation(op_sum, param1, param2)
            elif option_selected == 2:
                op_subtraction = Subtraction()
                execute_operation(op_subtraction, param1, param2)
            elif option_selected == 3:
                op_multiplication = Multiplication()
                execute_operation(op_multiplication, param1, param2)
            elif option_selected == 4:
                op_division = Division()
                execute_operation(op_division, param1, param2)
            else:
                print("Not executed")
        else:
            print("Invalid option selected")

    except Exception as e:
        print("Error Program", e)

    input("Press any key for continue..")



