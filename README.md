# Calculator Console Application in Python

## Overview
This project is a console application developed in Python. It includes a unit test using `unittest`. The Python version used is 3.12.2, and the project was developed using PyCharm IDE (Community Edition).

## Description
The application is a console-based calculator that allows users to perform basic arithmetic operations. Upon running the `main.py` file, users are presented with a menu of options to choose from. After selecting an option and entering the required values, the result is displayed. Users can continue performing operations or exit the program.

### Instructions
You must select one of the following options:
1. Sum
2. Subtraction
3. Multiplication
4. Division
0. Exit

Enter the option number you want to use: 1

First Value: 5

Second Value: 6

******************************************
Result of Sum: 11.0
******************************************

Press any key to continue...

## Running the Application
To run the application, execute the `main.py` file.

## Running Unit Tests
Unit tests for the calculator functionality are implemented in `test_calculator.py`. You can run the unit tests using a test runner or by executing the file directly.

## Requirements
- Python 3.12.2
- PyCharm IDE (Community Edition)

## License
This project is licensed under the [MIT License](LICENSE).
