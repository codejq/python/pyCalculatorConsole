class Calculator:
    def execute(self, value1, value2):
        """Execute math operation"""
        pass

    def __str__(self):
        return self.__class__.__name__


class Sum(Calculator):
    def execute(self, value1, value2):
        """Simple addition operation"""
        return value1 + value2


class Subtraction(Calculator):
    def execute(self, value1, value2):
        """Simple subtraction operation"""
        return value1 - value2


class Multiplication(Calculator):
    def execute(self, value1, value2):
        """Simple multiplication operation"""
        return value1 * value2


class Division(Calculator):
    def execute(self, value1, value2):
        """Simple division operation"""
        return value1 / value2


# Method used to execute the defined polymorphism operation
def execute_operation(operation, value1, value2):
    """Execute basic math operations"""
    result = operation.execute(value1, value2)
    print("******************************************")
    print(f"Result of {operation}: {result} ")
    print("******************************************")
